# Code Climate Govet Engine

`codeclimate-govet` is a Code Climate engine that wraps [govet](https://golang.org/cmd/vet). You can run it on your command line using the Code Climate CLI, or on our hosted analysis platform.

govet examines Go source code and reports suspicious constructs, such as Printf calls whose arguments do not align with the format string. Vet uses heuristics that do not guarantee all reports are genuine problems, but it can find errors not caught by the compilers.

### Installation

1. If you haven't already, [install the Code Climate CLI](https://github.com/codeclimate/codeclimate).
2. Add the following to your Code Climate config:
  ```yaml
  plugins:
    govet:
      enabled: true
  ```
3. Run `codeclimate engines:install`
4. You're ready to analyze! Browse into your project's folder and run `codeclimate analyze`.

## ➤ Requirements

- **[Docker](https://gitlab.com/megabyte-labs/ansible-roles/docker)**
- [CodeClimate CLI](https://github.com/codeclimate/codeclimate)

### Optional Requirements

- [DockerSlim](https://gitlab.com/megabyte-labs/ansible-roles/dockerslim) - Used for generating compact, secure images
- [Google's Container structure test](https://github.com/GoogleContainerTools/container-structure-test) - For testing the Docker images


### Building the Docker Container

Run the below make command from the root of this repository to create a local fat docker image
```shell
make image
```

### Building a Slim Container

Run the below make command from the root of this repository to create a local slim docker image
```shell
make slim
```


### Test

Run the below command from the root of this repository to test the images created by this repository.
```shell
make test
```

### Updating

`govet` is a part of the Go distribution and shares version with it. Once in a
while a new version of Go gets packaged. In order to get the latest version
and force a new docker image build, please update the base image in the
`Dockerfile`. Please avoid any unstable tags such as `latest` and keep it
explicit.

### Need help?

For help with Govet, [check out their documentation](https://golang.org/cmd/vet/).

If you're running into a Code Climate issue, first look over this project's [GitHub Issues](https://github.com/codeclimate/codeclimate-govet/issues), as your question may have already been covered. If not, [go ahead and open a support ticket with us](https://codeclimate.com/help).
