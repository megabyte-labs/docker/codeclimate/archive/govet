.PHONY: image

IMAGE_NAME ?= codeclimate/codeclimate-govet

SLIM_IMAGE_NAME ?= codeclimate/codeclimate-govet:slim

image:
	docker build --tag "$(IMAGE_NAME)" .

slim: image
	docker-slim build --tag $(SLIM_IMAGE_NAME) --http-probe=false --exec '/usr/src/app/codeclimate-govet' --mount "$$PWD:/code" --workdir '/code' $(IMAGE_NAME) && prettier --write slim.report.json

test: slim
	container-structure-test test --image $(IMAGE_NAME) --config tests/container-test-config.yaml && container-structure-test test --image $(SLIM_IMAGE_NAME) --config tests/container-test-config.yaml

